package com.tc.itfarm.api.enums;
/** 
 * @author  wdd 
 * @date 创建时间：2016年7月16日 下午3:09:53 
 */
public enum QueryArticleEnum {
	
	/** 浏览量查询 */
	PAGE_VIEW(" page_view desc"),
	/** 创建时间查询 */
	CREATE_TIME(" create_time desc"),
	/** 排序号查询 */
	ORDER_NO(" order_no desc"),
	/** 标题 */
	TITLE(" title asc"),
	/** 随机查询 */
	RANDOM(" rand()");
	
	public String criteria;
	
	QueryArticleEnum(String criteria) {
		this.criteria = criteria;
	}

	public String getCriteria() {
		return criteria;
	}

	public void setCriteria(String criteria) {
		this.criteria = criteria;
	}
	
	
}
