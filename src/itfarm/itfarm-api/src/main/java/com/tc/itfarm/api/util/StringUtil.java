package com.tc.itfarm.api.util;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by wangdongdong on 2016/8/21.
 */
public class StringUtil {

    private static Logger logger = LoggerFactory.getLogger(StringUtil.class);

    /**
     * 字符串分割成字符串集合
     * @param str
     * @param regex
     * @return
     */
    public static List<String> strToList(String str, String regex) {
        List<String> strList = Lists.newArrayList();
        if (StringUtils.isBlank(str) || StringUtils.isBlank(regex)) {
            logger.error("字符串或分割符不能为空!");
            throw new RuntimeException("字符串或分割符不能为空!");
        }
        String[] strs = str.split(regex);
        for (String s : strs) {
            strList.add(s);
        }
        return strList;
    }

    /**
     * 字符串分割成整数集合
     * @param str
     * @param regex
     * @return
     */
    public static List<Integer> strToNumberList(String str, String regex) {
        List<Integer> strList = Lists.newArrayList();
        if (StringUtils.isBlank(str) || StringUtils.isBlank(regex)) {
            logger.error("字符串或分割符不能为空!");
            throw new RuntimeException("字符串或分割符不能为空!");
        }
        String[] strs = str.split(regex);
        for (String s : strs) {
            strList.add(Integer.parseInt(s));
        }
        return strList;
    }

}
