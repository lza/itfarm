package com.tc.itfarm.api.common;
/**
 * 
 * @Description:代码常量
 * @author: wangdongdong  
 * @date:   2016年7月13日 下午3:07:45   
 *
 */
public final class Codes {
	private Codes(){
		
	}
	/** 分页数量 */
	public static final Integer COMMON_PAGE_SIZE = 6;
}
