/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50528
Source Host           : localhost:3306
Source Database       : it_farm

Target Server Type    : MYSQL
Target Server Version : 50528
File Encoding         : 65001

Date: 2016-08-29 19:07:44
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_article
-- ----------------------------
DROP TABLE IF EXISTS `t_article`;
CREATE TABLE `t_article` (
  `record_id` int(32) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `title_img` varchar(200) NOT NULL,
  `author_id` int(32) NOT NULL,
  `type_id` int(32) NOT NULL,
  `content` varchar(21299) NOT NULL,
  `keyword` varchar(100) DEFAULT NULL,
  `page_view` int(32) NOT NULL,
  `order_no` int(32) NOT NULL DEFAULT '0',
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `modify_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_article
-- ----------------------------
INSERT INTO `t_article` VALUES ('1', '程序员请放下你的技术情节，与你的同伴一起进步', '001.png', '1', '1', '如果说掌握一门赖以生计的技术是技术人员要学会的第一课的话， 那么我觉得技术人员要真正学会的第二课，不是技术，而是业务、交流与协作，学会关心其他工作伙伴的工作情况和进展...', null, '17', '0', '2016-07-13 14:22:12', '2016-07-13 14:22:14');
INSERT INTO `t_article` VALUES ('2', 'Java异常处理的陋习展播', '001.png', '6', '2', '作为一个Java程序员，你至少应该能够找出两个问题。但是，如果你不能找出全部六个问题，请继续阅读本文。\r\n\r\n \r\n\r\n本文讨论的不是Java异常处理的一般性原则，因为这些原则已经被大多数人熟知。我们要做的是分析各种可称为“反例”（anti-pattern）的违背优秀编码规范的常见坏习惯，帮助读者熟悉这些典型的反面例子，从而能够在实际工作中敏锐地察觉和避免这些问题。\r\n\r\n \r\n\r\n反例之一：丢弃异常 \r\n\r\n代码：15行-18行。\r\n\r\n这段代码捕获了异常却不作任何处理，可以算得上Java编程中的杀手。从问题出现的频繁程度和祸害程度来看，它也许可以和C/C++程序的一个恶名远播的问题相提并论??不检查缓冲区是否已满。如果你看到了这种丢弃（而不是抛出）异常的情况，可以百分之九十九地肯定代码存在问题（在极少数情况下，这段代码有存在的理由，但最好加上完整的注释，以免引起别人误解）。\r\n\r\n这段代码的错误在于，异常（几乎）总是意味着某些事情不对劲了，或者说至少发生了某些不寻常的事情，我们不应该对程序发出的求救信号保持沉默和无动于衷。调用一下printStackTrace算不上“处理异常”。不错，调用printStackTrace对调试程序有帮助，但程序调试阶段结束之后，printStackTrace就不应再在异常处理模块中担负主要责任了。\r\n\r\n丢弃异常的情形非常普遍。打开JDK的ThreadDeath类的文档，可以看到下面这段说明：“特别地，虽然出现ThreadDeath是一种‘正常的情形’，但ThreadDeath类是Error而不是Exception的子类，因为许多应用会捕获所有的Exception然后丢弃它不再理睬。”这段话的意思是，虽然ThreadDeath代表的是一种普通的问题，但鉴于许多应用会试图捕获所有异常然后不予以适当的处理，所以JDK把ThreadDeath定义成了Error的子类，因为Error类代表的是一般的应用不应该去捕获的严重问题。可见，丢弃异常这一坏习惯是如此常见，它甚至已经影响到了Java本身的设计。\r\n\r\n那么，应该怎样改正呢？主要有四个选择：\r\n\r\n1、处理异常。针对该异常采取一些行动，例如修正问题、提醒某个人或进行其他一些处理，要根据具体的情形确定应该采取的动作。再次说明，调用printStackTrace算不上已经“处理好了异常”。\r\n\r\n2、重新抛出异常。处理异常的代码在分析异常之后，认为自己不能处理它，重新抛出异常也不失为一种选择。\r\n\r\n3、把该异常转换成另一种异常。大多数情况下，这是指把一个低级的异常转换成应用级的异常（其含义更容易被用户了解的异常）。\r\n\r\n4、不要捕获异常。\r\n\r\n结论一：既然捕获了异常，就要对它进行适当的处理。不要捕获异常之后又把它丢弃，不予理睬。\r\n\r\n \r\n\r\n反例之二：不指定具体的异常 \r\n\r\n代码：15行。\r\n\r\n许多时候人们会被这样一种“美妙的”想法吸引：用一个catch语句捕获所有的异常。最常见的情形就是使用catch(Exception ex)语句。但实际上，在绝大多数情况下，这种做法不值得提倡。为什么呢？\r\n\r\n要理解其原因，我们必须回顾一下catch语句的用途。catch语句表示我们预期会出现某种异常，而且希望能够处理该异常。异常类的作用就是告诉Java编译器我们想要处理的是哪一种异常。由于绝大多数异常都直接或间接从java.lang.Exception派生，catch(Exception ex)就相当于说我们想要处理几乎所有的异常。\r\n\r\n再来看看前面的代码例子。我们真正想要捕获的异常是什么呢？最明显的一个是SQLException，这是JDBC操作中常见的异常。另一个可能的异常是IOException，因为它要操作OutputStreamWriter。显然，在同一个catch块中处理这两种截然不同的异常是不合适的。如果用两个catch块分别捕获SQLException和IOException就要好多了。这就是说，catch语句应当尽量指定具体的异常类型，而不应该指定涵盖范围太广的Exception类。\r\n\r\n另一方面，除了这两个特定的异常，还有其他许多异常也可能出现。例如，如果由于某种原因，executeQuery返回了null，该怎么办？答案是让它们继续抛出，即不必捕获也不必处理。实际上，我们不能也不应该去捕获可能出现的所有异常，程序的其他地方还有捕获异常的机会??直至最后由JVM处理。\r\n\r\n结论二：在catch语句中尽可能指定具体的异常类型，必要时使用多个catch。不要试图处理所有可能出现的异常。\r\n\r\n \r\n\r\n反例之三：占用资源不释放 \r\n\r\n代码：3行-14行。\r\n\r\n异常改变了程序正常的执行流程。这个道理虽然简单，却常常被人们忽视。如果程序用到了文件、Socket、JDBC连接之类的资源，即使遇到了异常，也要正确释放占用的资源。为此，Java提供了一个简化这类操作的关键词finally。\r\n\r\nfinally是样好东西：不管是否出现了异常，Finally保证在try/catch/finally块结束之前，执行清理任务的代码总是有机会执行。遗憾的是有些人却不习惯使用finally。\r\n\r\n当然，编写finally块应当多加小心，特别是要注意在finally块之内抛出的异常??这是执行清理任务的最后机会，尽量不要再有难以处理的错误。\r\n\r\n结论三：保证所有资源都被正确释放。充分运用finally关键词。反例之四：不说明异常的详细信息\r\n\r\n代码：3行-18行。\r\n\r\n仔细观察这段代码：如果循环内部出现了异常，会发生什么事情？我们可以得到足够的信息判断循环内部出错的原因吗？不能。我们只能知道当前正在处理的类发生了某种错误，但却不能获得任何信息判断导致当前错误的原因。\r\n\r\nprintStackTrace的堆栈跟踪功能显示出程序运行到当前类的执行流程，但只提供了一些最基本的信息，未能说明实际导致错误的原因，同时也不易解读。\r\n\r\n因此，在出现异常时，最好能够提供一些文字信息，例如当前正在执行的类、方法和其他状态信息，包括以一种更适合阅读的方式整理和组织printStackTrace提供的信息。\r\n\r\n结论四：在异常处理模块中提供适量的错误原因信息，组织错误信息使其易于理解和阅读。\r\n\r\n \r\n\r\n反例之五：过于庞大的try块 \r\n\r\n代码：3行-14行。\r\n\r\n经常可以看到有人把大量的代码放入单个try块，实际上这不是好习惯。这种现象之所以常见，原因就在于有些人图省事，不愿花时间分析一大块代码中哪几行代码会抛出异常、异常的具体类型是什么。把大量的语句装入单个巨大的try块就象是出门旅游时把所有日常用品塞入一个大箱子，虽然东西是带上了，但要找出来可不容易。\r\n\r\n一些新手常常把大量的代码放入单个try块，然后再在catch语句中声明Exception，而不是分离各个可能出现异常的段落并分别捕获其异常。这种做法为分析程序抛出异常的原因带来了困难，因为一大段代码中有太多的地方可能抛出Exception。\r\n\r\n结论五：尽量减小try块的体积。\r\n\r\n \r\n\r\n反例之六：输出数据不完整\r\n\r\n代码：7行-11行。\r\n\r\n不完整的数据是Java程序的隐形杀手。仔细观察这段代码，考虑一下如果循环的中间抛出了异常，会发生什么事情。循环的执行当然是要被打断的，其次，catch块会执行??就这些，再也没有其他动作了。已经输出的数据怎么办？使用这些数据的人或设备将收到一份不完整的（因而也是错误的）数据，却得不到任何有关这份数据是否完整的提示。对于有些系统来说，数据不完整可能比系统停止运行带来更大的损失。\r\n\r\n较为理想的处置办法是向输出设备写一些信息，声明数据的不完整性；另一种可能有效的办法是，先缓冲要输出的数据，准备好全部数据之后再一次性输出。\r\n\r\n结论六：全面考虑可能出现的异常以及这些异常对执行流程的影响。', null, '21', '0', '2016-07-13 16:02:22', '2016-08-29 19:05:37');
INSERT INTO `t_article` VALUES ('3', '一定要理解程序员的坏习惯', '001.png', '6', '1', '如果你是一个程序员,或者你就读计算机相关专业,那么你应该能理解下面这些诡异的小习惯是怎么养成的,或者你本身就有着其中的某些习惯:\r\n0.程序员数数会从0开始数起.\r\n例:程序员吵架的时候会说:“我数三下,你再不闭嘴我就不客气了!零!一!二!”或者列清单的时候编号会从0.开始写.\r\n因:array[0]是数组的第一个元素.\r\n评:这个习惯的养成是一个艰难的过程.多少次的越界,多少次的循环次数错误让我们深深的记住了从0开始.\r\n1.程序员用肯定和否定回答选择性的问题.\r\n例:问“您想喝咖啡还是喝茶?”程序员答:“嗯.”\r\n因:当条件或||有一个子句值为真,则整个语句值为真.\r\n评:这个习惯夸张了一点,但这的确是程序员思维方式的通病,用计算机的处理逻辑处理人类问题.当然这样的回答显然是存在编程错误的,因为对于这个问题他错误的返回了布尔型的值.虽然高度怀疑有没有人关心这种回答的结果是怎样,还是给出一个答案.如果真的在程序的世界里,服务员会优先考虑给你咖啡,如果没有咖啡了,才会端茶给你.\r\n2.程序员普遍患有或曾经患有分号结尾综合症;\r\n例:比如这几行;\r\n因:大部分程序,特别是多数程序员的启蒙编程语言C语言是用分号结尾语句的;\r\n评:换行之前习惯用分号结尾,这是程序员专属的坏习惯.还记得编译报的各种诡异的错最后原因总是少了那一个小小的”;”吗?这就是这个坏习惯的成因.据说分号结尾是有典故的,因为键盘上的分号正好在右手小拇指的位置,比起句号之类有着得天独厚的地理优势.当然也有不需要分号结尾的编程语言(比如Ruby还有Python),刚用起来会因为没有分号结尾而格外纠结.\r\n3.程序员对”语言”的概念和普通人有差.\r\n例:问:“你都会哪些语言?”普通人:“我会汉语和英语,还会一点点法语.”程序员:“我会C语言,C++和Java,还会一点点Ruby.”\r\n因:程序员的世界里没有母语和外语,只有编程语言.\r\n评:说真的,程序语言的语法学起来比外语更容易,拿到一个用没学过的语言写的程序,你多少可以看懂部分程序.程序语言没有国界,是各国程序员交流最好的桥梁.\r\n4.程序员格格外外的厌恶括号/引号不配对的情形.\r\n例:看到这样的内容(举个简单的例子(我知道你懂的),你能表示”我很淡定我不抓狂吗?\r\n因:各种不配对问题会导致编译错误.\r\n评:扫视代码,对不配对的情况异常敏感,瞥一眼就能发现哪里少了个右括号.这种症状在使用那些带自动补全功能的开发环境时变得更为纠结.\r\n5.程序员认为千(k)==1024.\r\n例:程序员换算表:1km==1024m,1kg==1024g.\r\n因:成因很曲折计算机是二进制的世界->1024是2的10次方->英语里用kilo一词来表示->kilo翻译成中文是”千”,比如千米,千克.\r\n评:也许一般人会认为500,1000是很整的数,但程序员会觉得512,1024是很整的数.不要奇怪,如果128,256,512这种形式看着不够整,帮你转换成二进制:10000000,100000000,1000000000…\r\n6.程序员惯用==来表示相等的意思.\r\n例:见5.\r\n因:在程序语言中,“=”是赋值,“==”才用来判断二者是否相等.\r\n评:单等和双等的区别是程序入门必修课,也是老师爱考的考点.如果在C语言if语句的判定条件里写了个单等,编译能通过,结果却往往不对,够一个新手查上一阵子的.当然目前的语言大都考虑到这一点,防止了这类的误写,但是单等和双等的差异已经深入每个程序员的内心.\r\n7.程序员使用”//”表示”请无视这一行文字”.\r\n例:餐厅中.程序员A:“我吃鱼香肉丝盖饭,你吃什么?”\r\n程序员B:“宫保鸡丁盖饭.”\r\n程序员A在点菜单写上:\r\n鱼香肉丝盖饭1\r\n宫保鸡丁盖饭1\r\n程序员B:“我还是要牛肉面吧!”\r\n程序员A更正点菜单:\r\n鱼香肉丝盖饭1\r\n//宫保鸡丁盖饭1\r\n牛肉面1\r\n因:程序中用”//”为代码加上注释,程序运行时会无视掉”//”开头的代码行.\r\n评:如果和一伙程序员在一起时被冷落了,可以抱怨一句”喂喂喂我是被注释掉了吗?”相信一向以幽默感丰富又有爱心著称的程序员们会关注你的.\r\n8.程序员有中文标点恐惧症.\r\n例:如果你注意到了,这篇文章自始至终都在使用英文标点,这就是最好的例子.\r\n因:程序中的全部符号都严格需要是英文半角标点.\r\n评:这个坏习惯是有中国特色的,中国程序员特有的.任何一名优秀的我国程序员都应该可以作到区别中文标点和英文标点.比如,和，还有’和‘.真的,当年初学编程的时候,诸如“error C2018:unknown character’0xa3′”的错误看得还不够多吗!\r\n就是这样一群人,如果你慢吞吞的用电脑他们会急躁的抢过键盘熟练的使用各种快捷键,他们会对着小孩子讲计算机的三原色是红绿蓝而不是红黄蓝,他们描述大小距离更偏向于以像素作单位,陪女朋友看电影看到电影院座位上稀稀落落的人就想做磁盘碎片整理…抱怨归抱怨,这不也正是他们的可爱之处吗!', null, '2', '0', '2016-07-13 17:37:39', '2016-08-12 17:42:12');
INSERT INTO `t_article` VALUES ('4', '程序员到底该如何学习？——初级', '001.png', '1', '2', '这篇文章只适合入门级的程序员。\r\n很多人都发消息问我，有没有推荐的学习、面试资料啊？哎，我只能说，《cracking the coding interview》、leetcode以及leetcode论坛，topcoder等。可是，这真的适合你吗？\r\n我真的不想做这样不负责任的回答。因为每个人的学习方法应该是不一样的，如果你真的想要获得成功，先来打磨下自己的学习方法，找到最适合自己的学习方法，你才能事半功倍。教一个婴儿和教一个成年人当然得用不一样的方法了。你得先花时间了解自己的学习能力才行。\r\n你有没有发现，有些人看起来花的时间比你少，可是他掌握知识的速度超级快？甚至有种过目不忘、一点就通的能力？如果你也能拥有超强的记忆力，对所学知识过目不忘，对一个问题一点就通，甚至还能举一反三，那慢慢地、慢慢地，你肯定会和别人拉开差距了。\r\n首先，了解你的大脑是如何学习的。\r\n一般来说，大脑学习一样东西，都是需要反复咀嚼的。一般第一次，能够有一个感性的认识，就是各个知识点我记住了。可是这个阶段的学习效果是不深刻的，很容易过段时间就忘记，而且也不能灵活运用，所以，需要不断的复习。每一次复习，你会巩固所学的知识，然后大脑在这个时期就对知识进行加工、改造，并总结规律。\r\n在这些简单、枯燥的重复的过程当中，有些人喜欢一次性就往脑子里塞很多东西，而有些人喜欢学习几十分钟然后休息几分钟，做点其他的事情，然后再继续学习。其实往往后者的效率会更高。因为在他们休息的时候，大脑也还在继续进行加工，这都是发生在潜意识中的。这叫作积极休息，你看到那些学一会就去打球、散步、买东西吃的同学们，他们不一定是贪玩。\r\n在你反复的积累一定的知识量之后，你再总结这些知识，会有一种融会贯通的灵感，会发现一些本质性的东西，能够很容易的举一反三，还能用简单的语言讲述给别人。这个时候，这些知识才真正的在你的大脑里扎根，他们已经被处理成非常精炼的几个知识要点而存储在大脑了，虽然有些细节随着时间流逝会忘记，但是这些精髓已经深深刻在你脑子里了，刻进了潜意识了，也才真正变成你的知识。有些人这个过程要短，有些人这个过程很长，这都是个体的差异，这些差异就在于每个人过去的深度学习的积累程度。\r\n为什么有些人高中数理化成绩非常好，读完大学就忘记了，但是他们头脑的逻辑推理能力还是很棒，就是因为他们的思维方式已经被扎根在大脑里了，那样学习新的知识的时候就能够轻易的把这些精髓的方法论调用出来。而且你会发现，他们哪怕是三四十岁之后，只要是运用逻辑推理方面的知识的，他们都能非常快的掌握，这就是大脑这部分能力被不断强化的结果。\r\n如果你和一个有舞蹈基础的人一起学跳舞，你就会很明显的感觉到，为什么老师一个动作示范一次他就完全掌握了，一个舞蹈教个两三次就全部记住了。因为他有基础，所以学习新东西的时候，其实新东西的总量和你比较就少很多了，他的大脑在相同时间需要处理的新事物也少很多，一个更高效的大脑处理更少的信息量，当然需要的时间就更短了。\r\n所以为什么有的人能够过目不忘？因为那些东西其实大部分早已经是他大脑的一部分了。\r\n学什么才能够锻炼超强大脑呢？\r\n那些学起来让你感到吃力，但是逼迫下自己还是可以坚持下去的精髓知识。简单的说，就是要折磨你的大脑，要挑战它的极限。你如果学习css、html等语言，肯定不如学习算法与数据结构、编译原理能折磨你的大脑。\r\n为什么大学课堂要把算法、编译原理、操作系统这样的课当作重点呢？因为他们是精髓，是你能够理解很多其他技术知识的基础。对于这些精髓，要肯下苦功，重复、重复再重复。今年你学习一次，也许理解得不够深刻，明年再复习一次，你花的时间就会变少，学习的效果也更好。\r\n请系统的学习。如果你总是碎片化的学习一些东西，恐怕你很难学得深入，请一个专题一个专题的大量的灌输知识。\r\n如果你想攻克一个领域的知识，你当然会想到Google一些学习资料、论坛，也知道来简书找“干货”，可是你知道如何判断什么才是精髓知识吗？看书，一定要看书，看好书，看经典。我们这些网络作家写的几千字的小散文能顶什么用呢？看经典的书籍是节省你的时间的。如果你连Google都还没学会，那就请一定要先学会如何Google。\r\n都互联网时代了，请运用互联网的思维来学习。互联网使得你获取信息的途径更多、更简单，你为什么还要来跟一个茫茫大海中的一个陌生人寻求学习方法呢？估且不说他没有能力教给你，就算他有那个实力，几分钟的交谈后给出的方法又怎么可能是为你量身定做的？\r\n那些你看起来很耗费时间的弯路，其实往往是捷径。\r\n只有勤奋\r\n什么培训班、速成班，结束了还是什么都不懂，都是一些人为了赚钱不负责任的说法。成为一个牛逼的hacker没有捷径。成为任何一个领域的高手都没有捷径。哪怕是天才，都是99％的汗水加上1％的灵感。你看那些聪明人好像学什么都轻松，那是因为他们在你还没醒悟的时候早已经付出了大量的艰辛和汗水。\r\n学会自学\r\n我以前总喜欢请教别人，好像别人都比我厉害似的，但是我现在更喜欢自己学习。那些大师写的书里都有答案。而且是互联网时代啊，我去麻烦别人、请教别人，还不如我自己去搜索、看书来的快、来的全面。\r\n跟随兴趣、积极反馈\r\n我觉得兴趣是最好的反馈。不管你是为了装逼而感兴趣，还是因为能够马上用到而去学习，还是只是单纯觉得好玩，都可以帮助你提高学习效率。当这个学习的过程是愉快的而非痛苦的，你会更容易坚持。当大脑不断的得到这样积极的反馈，潜意识你会觉得自己是一个非常强的学习者，从而帮助你走得更远。\r\n随时随地的学习\r\n不管你在做什么，都多想一想为什么，观察那些你不明白的地方。不管你和谁在一起，拼命的发现他身上值得你学习的地方。养成随时随地学习的好习惯，培养一颗“饥渴的大脑”。', null, '1', '0', '2016-07-13 17:38:47', '2016-07-14 18:41:47');
INSERT INTO `t_article` VALUES ('5', '刚挣钱的程序员同学该如何花钱？', '001.png', '1', '1', '我刚毕业那年第一个月到手的全额工资大概是 4k 出头，自己单独租了一个套一的房子再加上吃饭一个月 2.5k 就这么花掉了。每个月所剩余的可自由支配的钱确实不多，我这也算是在一线城市的广深地区，在内地二线城市的朋友当时只有 1k 出头，合租房加上吃饭还略显紧张。所以我们当时不太需要考虑花钱这个事，确实是没什么钱可花的。\r\n今天行业发展得不错，如今程序员的起薪大幅提升，刚走出校门的同学除租房吃饭之外想必都还有不少剩余可供花销。作为一个过来人，写这篇文章其实就想谈谈早期如何更明智的花钱，可能会对自身未来的发展更有好处。\r\n电脑\r\n\r\n一般进入公司你会得到一台工作电脑，有些公司是直接给你配置发放没得选择（当年我就是领了个二手本），有些是给你一个上限额度让你自己去买一台，再分几年折旧完，比如额度 8k 分四年折旧，这样你每在公司呆满一年，公司返 2k 给你。我比较喜欢后面一种，这样我可以自由选择自己的电脑配置，但偏偏我这十年工作过的所有公司都是第一种，只能呵呵了。\r\n工作前几年公司发什么电脑我只好用什么，工资剩余也不多，当时虽然心系 ThinkPad T 系列的本子，但按我前面的每月结余不得存上一年才能买得起啊。而且心想我干嘛要拿自己电脑去公司工作啊，当时确实想不通。所以结果就是工作的前五年，我换了三个公司用了三个笔记本，都有一个共同特点：慢，还老需要重装系统。回头一想浪费了多少青春好时光在等待编译、打包和重装系统上啊。\r\n五年前到了京东后，碰到了一个同事他就用的自己的电脑，一台 09 款的苹果小白 Macbook。这是最后一代塑料外壳的 Mac，之后都是细磨砂铝合金的了。之后他就一直给我安利 Mac 的好处，然后我就自己买了一台当时最新的 Macbook Pro，再 DIY 换了 SSD 硬盘，原机搭配的机械硬盘用作 TimeMachine 的备份盘，再把内存加到 12G 一直用到今年初。再更新了最新的 OS X EI Capitan 之后，感觉系统变得更快了（说明 OS 优化的更好了吧）。\r\n用 Mac 的五年里彻底告别了 Windows 下重装系统的问题，都快忘记怎么装了。在原来公司发的电脑上 mvn package 一下一个以前的 Java 工程需要 50 秒，在我的 Mac 下是 20 秒。考虑下这五年来这个命令敲击了多少次，每次都节省半分钟，还是很可观的。关于为什么要用 Mac 的最有力的理由，我觉得最有发言权的是 Mac 代言人池建强在他的文章《先有 Mac 还是先有银元？》里已经写得很明白了，我就不再多说了。\r\n椅子\r\n\r\n去年底去体检时医生说我的颈椎有轻微变形，而我自己也时常感受到肩背的酸痛。既然达到了变形的程度，肯定非一日之功，都是长年累月不正确的坐姿导致的。程序员进入专注写代码和调程序的状态，或者全神贯注的处理着线上的问题和 bug，哪里还会注意到自身坐姿的问题。有时还要加班熬夜通宵什么的，累了就趴在桌上睡着了，这些都是导致脊椎长期过载并变形的原因，然后随之而来就是肩背酸痛，扭动脖子咔咔得响。\r\n正因为如此我才开始关心起椅子的事情，然后我想到好些年前看的《软件随想录》，其中程序员部落酋长 Joel 写道：\r\n程序员的椅子是 Aerons 出品的名牌电脑椅还是 Staples 品牌打折时的便宜货？关于 Herman Miller 设计的著名的 Aeron 牌电脑椅，请让我在这里多说一句。\r\n这种椅子的价格是每把 900 美元，比办公用品连锁超市 Office Depot 里卖的便宜货或者 Staples 牌贵出 800 美元。Aeron 牌电脑椅比那些便宜货舒服得多。如果尺寸正确，并且调节到最合适的位置，那么大多数人坐一整天都不会感到不舒服。靠背和坐垫都被设计成网状形，空气可以自由流通，所以坐着不会捂汗。这种椅子包含了第一流的人体工程学设计，尤其是那些带有腰部支撑的新型号。 它们比便宜的椅子更耐用，一把 Aeron 牌电脑椅的寿命至少抵得上 4 把那种 100 美元的椅子。\r\n所以结果是，买一把 Aeron 牌电脑椅，每 10 年只多出 500 美元，也就是每年多出 50 美元，相当于在每个程序员身上每星期多支出 1 美元。一卷上等卫生纸的价格大约是 1 美元。你的程序员每人每星期大概会用掉一卷。所以，将电脑椅升级到 Aeron 牌，多出的花销与你花在程序员的卫生纸上的开销大致相等。但是，我可以向你保证，如果你把卫生纸的支出拿到预算委员会上讨论，你一定会被严厉地呵斥不要捣乱，还有许多更重要的事需要讨论。\r\nAeron 的椅子确实很好，它曾“坐”揽诸多设计大奖，被美国现代艺术博物馆列为永久典藏，还入选了由伦敦设计博物馆甄选的顶尖案例集《50把改变世界的椅子》，堪称办公椅设计史上当之无愧的“头把交椅”。当然价钱也很感人，国内在万元附近，所以基本上绝大多数公司都不会给你配备类似这样的办公座椅。大部分公司配备都是几百块一把的普通办公椅，无人体工学设计，在工作时对你的肩背和脊椎没有任何支撑作用。\r\n不过像 Aeron 这样高端的椅子我自己买起来都还是感觉肉痛，但至少给自己配一把符合人体工学设计的椅子还是有必要的。在 1.5k 到 3k 之间还是有一些可供选择的替代品，别等到十年后需要去医院做脊椎牵引理疗，这花费的时间和金钱不比椅子贵多了，还要受痛苦。当然希望未来更多公司都能想清楚即使一把顶级的 Aeron 椅子的价钱相比程序员的人力成本也算不了什么吧。\r\n房子\r\n\r\n如果再给我一次机会，我毕业的那年就该想尽办法把房子买了，那么是否意味着今年毕业的你也需要立刻买房呢？斗转星移，时移势易，房产的黄金十年是否已经过了？我 12 年底才在二线城市成都买了套房，每平米价格 7、8k 和我刚毕业那年广州天河岗顶附近差不多。几年过去了这套房子的价格依然保持平稳，没涨没跌。\r\n2016 年伊始就听说上海虹口区新楼盘遭哄抢一空，352 套房源一天内售罄，平均每套均价近千万，每平米均价 8 万。现在这种一线城市的千万级住房都算不上什么豪宅，只不过是普通的小区住宅，是否值得购买呢？基本上我看还是算了吧，如果房子总价超过你年薪的十倍，那么还是别用贷款上杠杆买房了。\r\n如果是在二线城市，比如我所在的成都，不说一千万，一百万的房子已经有很大的选择余地。如果你有年薪十万那么贷款买一套总价百万附近的首套自住房，看起来还算比较合理。现在库存房源太多，首套房贷款比例已降到两成，而公积金贷款的利率也比较低，尽可能长（30 年，因为钱总是不断贬值的）的还贷期限，那么双工薪阶层夫妻双方的公积金就可能能覆盖了每月的还贷额，对生活开支的影响微乎其微。\r\n理财\r\n\r\n在买完上面这些后，剩下的节余就用来该用来理财。李笑来写过一篇《越早开始越好的事情没几个：理财排在第一位》，他对于理财的观点如下：\r\n所谓的理财，理论上并不应该狭义地理解为去银行买理财产品。\r\n存钱、做预算、控制开销、赚更多利息、赚利息差、正确使用信用卡、购置不动产、投资一些多少有风险的标的，这些都是理财活动。\r\n所谓理财，这个定义比较合理准确：如何有效管理现金流 —— 这其实与钱多钱少关系不大。\r\n深以为然，对于理财我就属于开窍得挺晚的，但我观察了下在我身边一直没开窍的也还挺多的，而我的上一代人几乎从来就没有开窍过，一生唯一的理财方式就是银行存款。既然提到理财就是有效管理现金流，我就谈谈如今我管理现金流最重要的一条，永远不把钱放在银行活期账户上。\r\n所以每次工资一到账，预算好本月需要还的各种账单和可预期支出，这部分就会立刻就转到余额宝。其余的部分转入流动性比余额宝更差点，但利息更高的风险资产。理财的本质就是聪明的承担风险，这里的「聪明」并非智商高下，更多是一种控制能力，风险控制和自我心理的控制。至于如何才能变聪明，除了不断学习、试错并吸收经验教训，然后永远地重复这个过程。所以知道为什么越早开始越好了吧，至少试错的成本更低。\r\n大部分人对理财的关心都放在了怎么存来获取更高的利息或投资收益，其实理财的另一面是怎么花。关于花钱就自然绕不开信用卡，传统的观念是挣多少花多少，绝不用信用卡今天花明天的钱。这种观念绝不是什么老一代人的观念，其实我观察到比我年轻的好些人都有。如果你控制不住刷信用卡并导致入不敷出，成为卡奴。那么其实可能你的自控能力也不足也去承担更高风险的理财活动。\r\n虽然我 04 年就办了第一张信用卡，但却一直没开窍用得一塌糊涂。我认为刷信用卡的一大作用是积攒信用，而信用在未来会成为你的财务杠杆。另外刷信用卡还有很多「羊毛」可以薅，有刷信用卡送加油洗车的，有超市或海淘刷卡返现的，也有刷卡送星巴克咖啡或电影票的，还也有机场接送或免费航空保险等等，而且大部分的信用卡刷卡都有积分，积分又可以兑换礼品或航空里程。这里面的窍门多多，但我觉得不要为了薅羊毛去花太多心思，办太多卡，这里面的时间成本也不低。所以结合自身的消费习惯和生活所需去适当办理几张信用卡还是不错的。\r\n这时可能有个观念又会跳出来挑战，手上不留点应急救命钱么？不用，前面说了信用这时就可以当钱用。随时可以从手上的信用卡、京东金条、支付宝借吧、腾讯微粒贷、消费信贷之类的地方借出需要应急救命的钱。而你实际有那么多需要应急救命的时刻么？\r\n买书\r\n\r\n这条最重要，但只有两句话，所以放在最后。买书根本不需要考虑价钱，读书最大的成本是选书和读所花费的时间成本。所以别花时间去找这个资源那个下载了，直接买是最划算的方式。\r\n...\r\n写了这么些方面，其实这些都是我刚开始挣钱时都没做好的地方，终于开窍了，时间已经过去了不少。', null, '0', '0', '2016-07-13 17:39:41', '2016-07-13 17:39:43');
INSERT INTO `t_article` VALUES ('6', '程序猿啊你闷骚又迷人', '001.png', '6', '1', '一\r\n话说，当你一严（dou）肃（be）脸地对程序猿说：你很闷骚耶！会收到对方一个360度的鄙视：“你才闷骚，你全家都闷骚。”\r\n此时你震惊了，闷骚不是褒义词嘛？\r\n闷骚：喻人外表克制、中规中矩，内心却充满激情、渴望。\r\n瞧瞧，人家的官方解释如是也，闷骚=做人低调做事高调。\r\n古有风骚，今有闷骚。\r\n广大猿类一天12小时像对着初恋一样对着电脑，待人接物腼腆害羞，因而大多数是“闷”的，而看到他们酷炫的作品、神秘的背影，就会被冠以“骚”字。\r\n所以，亲爱的程序猿媛们，说你闷骚，其实是在夸你呢！\r\n二\r\n作为理工科的子类，程序猿从而继承了它的所有属性。\r\n他们有些确实挺闷的，话少，爱宅，爱游戏，爱看书，爱在家鼓捣各种东西；不爱运动，甚至社交活动接近于零。\r\n面对心仪女神爱在心中口难开，差点憋出内伤。\r\n私下很黄很暴力，分分钟从网络的海洋中找到单身狗梦寐以求的种子，或者很大方拍拍你肩膀，说，硬盘给你。\r\n若是有事没事天天出去浪，一周不碰代码，就会饥渴难耐。\r\n热忱写代码，衣带渐宽，但仍废寝忘食去撸代码。\r\n经常对亲戚朋友们友情赞助修电脑、下电影、申请QQ号、装wifi等，在外人眼中的高大上技能get，他却不屑一顾。\r\n低调淡漠的外表，丰富激情的内心。\r\n三\r\n有些猿类擅长自黑，满口黄段子，是团队中调节气氛的良剂，随便说一丢，能笑翻你。\r\nPM 频繁更改需求，他会很憋屈吐槽：宝宝心里苦，但宝宝不说，祖国的花朵就这么毁在你手里了。\r\nQA 一大波的 BUG 来袭，会委屈叫到：你们欺负我这个小公举。\r\n有些170+的汉子程序猿小盆友喜欢看萌系漫画，电脑桌面背景图片炒鸡卡哇伊，卖萌指数飙升。\r\n他们喜欢坐着，但思想是奔跑的，有时候你无法进入他们的世界。\r\n他们会淡定面对一大波过来的BUG；一而再再二三变更的需求，虽然吐槽不断，但最后还会埋头苦干；面对小白用户提出的小白问题，内心鄙视一万次然后会认真回答。\r\n话少把事做好，领导最爱。\r\n闲的时候骚而不闷，忙的时候只闷不骚，这样的程序猿，挺迷人哒。\r\n四\r\n有心上人的猿类，会对所有人闷，却只对她/他骚，程序猿的浪漫只有被表白的人懂。\r\n在网上有各种炫耀自家猿类男神的：\r\n曾有个猿类，背地里偷偷做一个网站，上面记录着和女友的点点滴滴，女友看了hin感动。\r\n也曾有个猿类，女友喜欢看段子，他就写了一些爬虫程序，把热门网站上的好玩的图片和段子都挖掘来，有事没事给女盆友发，且一发一大把，分分钟把女友笑翻，于是女盆友的马甲线就这么练。粗。来。了。\r\n也曾有个猿类，手把手教老婆翻墙，帮忙找参考资料，关键是找的资料都细而全，作用大大滴。\r\n......\r\n不同于花里胡哨的浪子，他们更愿意用行动证明一切。\r\n那些既屌丝又装逼的金融男，既花心又没脑的销售狗，还有各种傲娇的文艺直男，你们粗来，保证不打你！\r\n五\r\n无数男子倾倒在程序媛的石榴裙下，为她们遇到问题的谨慎淡定，为她们精密的逻辑和思维，为她们不撒娇不做作......\r\n认真做家务的男人很帅，认真写代码的妹纸更加迷人。\r\n她们颜值有辨识度，不是风华绝代，但也有足够震撼力，码出 PM 求之不得的酷炫功能。\r\n进可欺身压海棠，退可提臀迎蛟龙，酱紫的程序媛，怎能令人不爱？\r\n六\r\n知乎上有人回答说：“闷骚”对程序猿来说是一种高尚的品质，因为他们遇事理性，也善于学习生活。而程序猿大多数集这些优良品质于一身，奔跑在升职加薪的道路上，发出一声猿叫~嗷～～～\r\n你身边要是有这种低调闷骚又迷人的猿媛，赶紧走进他们内心的世界吧，去挖掘他，你会被萌翻啊，发现他们一点也不闷。\r\n单身的你，遇到好的，就不要错过啊啊啊！\r\nPS：程序猿中也不乏有“败类”，如搞三角恋、劈腿、家暴、小气自私。谁能解释下他们是怎么混进来的，如果还能遇到这种货色，上辈子真的是折断了天使的翅膀才会碰到啊。\r\n遇到这种“异教徒”，分分钟pia飞到墙上（抠不下来的那种），好不呢？', null, '0', '0', '2016-07-13 17:40:35', '2016-07-13 17:40:37');
INSERT INTO `t_article` VALUES ('7', '为什么程序员的工作效率跟他们的工资不成比例', '001.png', '1', '1', '<span style=\"color:#454545;font-family:Verdana, \'Microsoft YaHei\';font-size:16px;line-height:30px;background-color:#FFFFFF;\">最有效率的程序员会比一般的程序员的编程效率高上几个数量级。但在任何公司里，他们的工资水平却只会出现很小的浮动差距。甚至在整个行业内，这种差距也不是很大。如果一个程序员的效率能达到其他人的10倍，为什么他不能得到10倍高的报酬呢？</span><br />\r\n<br />\r\n<p style=\"font-family:Verdana, \'Microsoft YaHei\';color:#454545;font-size:16px;background-color:#FFFFFF;\">\r\n	Joel Spolsky在最近的他的一个演讲里就这个问题给出了一系列的答案。首先，程序员的工作效率在整个行业内千差万别，但在一个公司里却不会有太大的差距。如果一个人比他的同事的效率高10倍，那他基本上会离开，要么去找更有天赋的人一起工作，要么去创业开公司。第二，极高的工作效率并没有被察觉。这篇文章我们主要讨论这第二种情况。\r\n</p>\r\n<p style=\"font-family:Verdana, \'Microsoft YaHei\';color:#454545;font-size:16px;background-color:#FFFFFF;\">\r\n	<br />\r\n</p>\r\n<p style=\"font-family:Verdana, \'Microsoft YaHei\';color:#454545;font-size:16px;background-color:#FFFFFF;\">\r\n	一个人的工作效率比同伴高10倍怎么可能不被察觉呢？在某些行业里，这种差别是显而易见的。一个销售人员的效率是他的同伴的10倍，这很容易看出来，而且他也能得到相应的报酬。销售成绩容易测量，就比如有些销售人员销售额会是其他人的数十倍。一个泥瓦匠的工作效率是其同伴的10倍，这也很容易看出，但这种事情不会发生：最好的泥瓦匠也不可能比一个一般的泥瓦匠快10倍。软件的产出不可能像销售或砌砖那样容易的测量。最好的程序员并不是能写10倍多的代码，他们也不是能多干10倍多小时的工作。\r\n</p>\r\n<p style=\"font-family:Verdana, \'Microsoft YaHei\';color:#454545;font-size:16px;background-color:#FFFFFF;\">\r\n	<br />\r\n</p>\r\n<p style=\"font-family:Verdana, \'Microsoft YaHei\';color:#454545;font-size:16px;background-color:#FFFFFF;\">\r\n	程序员在避免写代码时才体现出最高的效率。他们能认识到人们要求他们解决的问题并不需要解决，他们能知道客户并不清楚自己想要的东西是什么。他们知道什么地方可以重复利用或可修改来解决问题。他们会欺骗客户。但是，当他们做到了最高的效率时，没有人会说“哇塞！这比用那笨办法做会省事100倍。你应该涨工资。”顶多人们会说“这个主意不错！”然后继续干活。你需要很长的时间才能发现有些人经常性的显现出省时省力的睿智。或者反过来说，你需要很长的时间才能认识到有些人虽然经常加班加点的编程却没有什么产出。\r\n</p>\r\n<p style=\"font-family:Verdana, \'Microsoft YaHei\';color:#454545;font-size:16px;background-color:#FFFFFF;\">\r\n	<br />\r\n</p>\r\n<p style=\"font-family:Verdana, \'Microsoft YaHei\';color:#454545;font-size:16px;background-color:#FFFFFF;\">\r\n	对超级程序员形象的一种具有浪漫主义色彩的描绘是：他开启Emacs编辑器，敲起代码来就像打机枪，把一个软件从无到有开发成没有任何缺陷的最终产品。而一个更精确的描绘是：他静静的望着太空几分钟，然后说“咦，这好像以前在什么地方见过呀。”\r\n</p>', null, '2', '0', '2016-07-14 17:44:40', '2016-08-12 17:42:33');
INSERT INTO `t_article` VALUES ('8', '测试', '001.png', '1', '1', '测试<img src=\"/itfarm-web/attached/image/20160714/20160714174558_88.jpg\" alt=\"\" />', null, '2', '0', '2016-07-14 17:46:00', '2016-07-14 18:36:15');
INSERT INTO `t_article` VALUES ('9', '程序员加班如何谈效率', '001.png', '1', '1', '<img src=\"http://www.phpxs.com/uploads/201605/07/14626080831.jpg\" alt=\"加班\" />\r\n<p style=\"font-family:\'PT Serif\', Georgia, Times, \'Times New Roman\', serif;font-size:18.4px;vertical-align:baseline;color:#222222;background-color:#F8F8F8;\">\r\n	最近听人谈起程序员的加班问题，忙到那么晚，真有那么多事情要做么？当然每个人遇到的情况不一样，给到的答案也是不一样的，这里针对个人所遇到的情况，谈谈对加班的一些看法。\r\n</p>\r\n<h2 style=\"font-weight:normal;font-family:\'PT Serif\', Georgia, \'Helvetica Neue\', Arial, sans-serif;font-size:20px;vertical-align:baseline;color:#222222;background:#FFFFFF;\">\r\n	任务量是不是很多？\r\n</h2>\r\n<p style=\"font-family:\'PT Serif\', Georgia, Times, \'Times New Roman\', serif;font-size:18.4px;vertical-align:baseline;color:#222222;background-color:#F8F8F8;\">\r\n	答： 是的。因我们最近涉及到改版，将旧有的逻辑全部以新的格式重新书写；以及引入的新的框架、架构，有许多的东西要学，这样就无形中也增加了任务量。但这就是加班的主要理由吗？\r\n</p>\r\n<h2 style=\"font-weight:normal;font-family:\'PT Serif\', Georgia, \'Helvetica Neue\', Arial, sans-serif;font-size:20px;vertical-align:baseline;color:#222222;background:#FFFFFF;\">\r\n	核心问题-效率\r\n</h2>\r\n<p style=\"font-family:\'PT Serif\', Georgia, Times, \'Times New Roman\', serif;font-size:18.4px;vertical-align:baseline;color:#222222;background-color:#F8F8F8;\">\r\n	结合个人在做任务开发时所遇到的问题以及所浪费的时间，思考了许久。发现真正的问题，是在做任务写代码时，是否是以较高的效率来完成并解决问题。但说起效率这一点，又是有些大的概念，具体以下几个方面谈起：\r\n</p>\r\n<h3 style=\"font-family:\'PT Serif\', Georgia, \'Helvetica Neue\', Arial, sans-serif;font-size:1.3em;vertical-align:baseline;color:#222222;background-color:#F8F8F8;\">\r\n	1. 分解任务，理清思路\r\n</h3>\r\n<ul style=\"font-family:\'PT Serif\', Georgia, Times, \'Times New Roman\', serif;font-size:18.4px;vertical-align:baseline;color:#222222;background-color:#F8F8F8;\">\r\n	<li style=\"font-weight:inherit;font-family:inherit;font-style:inherit;font-size:18.4px;vertical-align:baseline;\">\r\n		我们在拿到任务时，不能草率对待，直接开始上手。而应该对过大的任务做一些任务分解，分解为一个个在一天内以小时为单位可以完成的任务。另外，对需要花时间调研的，也要将其分解为任务。\r\n	</li>\r\n	<li style=\"font-weight:inherit;font-family:inherit;font-style:inherit;font-size:18.4px;vertical-align:baseline;\">\r\n		在每个任务的开发之前，一定要确保思路的清晰。如何保证，可以采取一些工具来帮助，例如，思维导图来帮助我们记录我们在完成任务时，需要考虑的细节点，其清晰的结构对我们任务的理解是极大好处的；若是当前的任务可以涉及到的逻辑比较复杂或者状态比较多的话，这是就得考虑使用UML建模工具，其中用来记录对象的状态变换的状态图，理清程序的逻辑的活动图，清晰对象交互的时序图则是我们常用且必不可少的。\r\n	</li>\r\n	<li style=\"font-weight:inherit;font-family:inherit;font-style:inherit;font-size:18.4px;vertical-align:baseline;\">\r\n		万事开头难。可能在开始进行这些工作时，我们会不熟悉，而感觉比较麻烦，又或者画出的图带着些缺陷。但请确信，坚持下去并慢慢总结自己遇到的问题，会对我们开发或者做事会带来极大的便利的。毕竟，这时你要是做的很好了，就可以直接作为一份开发文档了，比起代码来说，不是容易理解多了吗？\r\n	</li>\r\n</ul>\r\n<h3 style=\"font-family:\'PT Serif\', Georgia, \'Helvetica Neue\', Arial, sans-serif;font-size:1.3em;vertical-align:baseline;color:#222222;background-color:#F8F8F8;\">\r\n	2. 预留时间，以防踩坑\r\n</h3>\r\n<p style=\"font-family:\'PT Serif\', Georgia, Times, \'Times New Roman\', serif;font-size:18.4px;vertical-align:baseline;color:#222222;background-color:#F8F8F8;\">\r\n	对若是对任务的完成把握性不大，可以多预估一点时间。但当我们的技能越来越熟练的时候，对任务的时间度把握性也会愈加的精准。\r\n</p>\r\n<h3 style=\"font-family:\'PT Serif\', Georgia, \'Helvetica Neue\', Arial, sans-serif;font-size:1.3em;vertical-align:baseline;color:#222222;background-color:#F8F8F8;\">\r\n	3. 及时饶坑，任务优先\r\n</h3>\r\n<p style=\"font-family:\'PT Serif\', Georgia, Times, \'Times New Roman\', serif;font-size:18.4px;vertical-align:baseline;color:#222222;background-color:#F8F8F8;\">\r\n	在遇到问题的时候，若是在预定的时间内没得到解决的话，则必须要进行求助，或者将其先放置，完成其他任务优先。\r\n</p>\r\n<h3 style=\"font-family:\'PT Serif\', Georgia, \'Helvetica Neue\', Arial, sans-serif;font-size:1.3em;vertical-align:baseline;color:#222222;background-color:#F8F8F8;\">\r\n	4. 深入学习，防患未然\r\n</h3>\r\n<p style=\"font-family:\'PT Serif\', Georgia, Times, \'Times New Roman\', serif;font-size:18.4px;vertical-align:baseline;color:#222222;background-color:#F8F8F8;\">\r\n	这样，下班之后的时间，我们便可以去充分地利用。对工作中所需要的技能点去做一些深入地学习。又或者对工作中没能得到解决的任务和困难点，做一些研究总结性的东西。又或者对知识点去进行系统性地学习。这里的时间，是我们提高和丰富自己的时候，需要充分地利用。\r\n</p>\r\n<h3 style=\"font-family:\'PT Serif\', Georgia, \'Helvetica Neue\', Arial, sans-serif;font-size:1.3em;vertical-align:baseline;color:#222222;background-color:#F8F8F8;\">\r\n	5. 番茄工作，劳逸结合\r\n</h3>\r\n<p style=\"font-family:\'PT Serif\', Georgia, Times, \'Times New Roman\', serif;font-size:18.4px;vertical-align:baseline;color:#222222;background-color:#F8F8F8;\">\r\n	人的一天的工作精力是有限的，则需要对自己的精力进行一些规划。在自己高效地时间，尽可能地完成主要的工作。同时，也要进行工作一定的时间，休息几分钟，调整一下自己的状态。\r\n</p>\r\n<h2 style=\"font-weight:normal;font-family:\'PT Serif\', Georgia, \'Helvetica Neue\', Arial, sans-serif;font-size:20px;vertical-align:baseline;color:#222222;background:#FFFFFF;\">\r\n	总结\r\n</h2>\r\n<p style=\"font-family:\'PT Serif\', Georgia, Times, \'Times New Roman\', serif;font-size:18.4px;vertical-align:baseline;color:#222222;background-color:#F8F8F8;\">\r\n	上面谈及的工作效率的一些点，是结合了自己的经验之谈，如有问题，欢迎指教讨论。另外，还有一些想表达的是，加班是相当不必要的，毕竟工作不能占了我们生活的全部。工作时间之余，应该是我们提高自己，提升并丰富自己生活的质量。\r\n</p>', null, '3', '0', '2016-07-14 18:31:56', '2016-07-14 18:36:01');
INSERT INTO `t_article` VALUES ('10', 'ww', '14709157250630jpg', '1', '1', 'weqqwewq', '213', '6', '0', '2016-08-11 19:42:05', '2016-08-12 17:42:02');

-- ----------------------------
-- Table structure for t_category
-- ----------------------------
DROP TABLE IF EXISTS `t_category`;
CREATE TABLE `t_category` (
  `record_id` int(32) NOT NULL AUTO_INCREMENT,
  `parent_id` int(32) DEFAULT NULL,
  `name` varchar(200) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(32) NOT NULL,
  `modify_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_category
-- ----------------------------
INSERT INTO `t_category` VALUES ('1', null, '程序人生', '2016-08-15 15:32:54', '1', '2016-08-15 15:32:53');
INSERT INTO `t_category` VALUES ('2', null, '技术', '2016-08-15 15:32:57', '6', '2016-08-15 15:32:56');
INSERT INTO `t_category` VALUES ('3', null, '扯淡', '2016-08-15 15:32:59', '1', '2016-08-15 15:32:58');
INSERT INTO `t_category` VALUES ('4', null, '213', '2016-08-15 15:33:03', '1', '2016-08-15 15:33:01');
INSERT INTO `t_category` VALUES ('5', null, 'd', '2016-08-15 15:33:04', '1', '2016-08-15 15:33:01');
INSERT INTO `t_category` VALUES ('6', null, 'd', '2016-08-15 15:33:05', '1', '2016-08-15 15:33:01');
INSERT INTO `t_category` VALUES ('7', null, 'w', '2016-08-15 15:33:05', '1', '2016-08-15 15:33:01');
INSERT INTO `t_category` VALUES ('8', null, 'w', '2016-08-15 15:33:06', '1', '2016-08-15 15:33:01');
INSERT INTO `t_category` VALUES ('9', null, '321', '2016-08-15 15:33:07', '1', '2016-08-15 15:33:01');
INSERT INTO `t_category` VALUES ('10', null, '321', '2016-08-15 15:33:07', '1', '2016-08-15 15:33:01');
INSERT INTO `t_category` VALUES ('11', null, '321', '2016-08-15 15:33:08', '1', '2016-08-15 15:33:01');
INSERT INTO `t_category` VALUES ('12', null, '321', '2016-08-15 15:33:09', '1', '2016-08-15 15:33:01');
INSERT INTO `t_category` VALUES ('13', null, '321', '2016-08-15 15:33:10', '1', '2016-08-15 15:33:01');

-- ----------------------------
-- Table structure for t_log
-- ----------------------------
DROP TABLE IF EXISTS `t_log`;
CREATE TABLE `t_log` (
  `record_id` int(32) NOT NULL AUTO_INCREMENT,
  `type` varchar(200) NOT NULL COMMENT '日志类型',
  `old_content` varchar(10000) DEFAULT NULL COMMENT '修改前内容',
  `new_content` varchar(10000) DEFAULT NULL COMMENT '修改后内容',
  `user_id` int(32) NOT NULL COMMENT '操作人员id',
  `username` varchar(100) NOT NULL COMMENT '操作人员用户名',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modify_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_log
-- ----------------------------
INSERT INTO `t_log` VALUES ('19', '删除Integer', '11', null, '1', '爱吃猫的鱼', '2016-08-17 16:54:25', '2016-08-17 16:54:25');
INSERT INTO `t_log` VALUES ('20', '新增Role', null, '{\"recordId\":12,\"name\":\"135\",\"remark\":\"12\",\"createTime\":1471424292662,\"modifyTime\":1471424292662}', '1', '爱吃猫的鱼', '2016-08-17 16:58:13', '2016-08-17 16:58:13');
INSERT INTO `t_log` VALUES ('21', '新增Role', null, '{\"recordId\":13,\"name\":\"wetew\",\"remark\":\"23\",\"createTime\":1471424473348,\"modifyTime\":1471424473348}', '1', '爱吃猫的鱼', '2016-08-17 17:01:13', '2016-08-17 17:01:13');
INSERT INTO `t_log` VALUES ('22', '修改Role', null, '{\"recordId\":13,\"name\":\"wetew\",\"remark\":\"23\",\"createTime\":null,\"modifyTime\":1471424520198}', '1', '爱吃猫的鱼', '2016-08-17 17:02:00', '2016-08-17 17:02:00');
INSERT INTO `t_log` VALUES ('23', '修改User', null, '{\"recordId\":12,\"username\":\"wdd123\",\"telephone\":\"18888888888\",\"sex\":1,\"password\":null,\"age\":22,\"status\":1,\"address\":\"安徽灵璧\",\"photo\":null,\"email\":\"asdds\",\"qq\":\"812908087\",\"nickname\":\"爱吃猫的鱼\",\"registerTime\":null,\"modifyTime\":1471424572235}', '1', '爱吃猫的鱼', '2016-08-17 17:02:52', '2016-08-17 17:02:52');
INSERT INTO `t_log` VALUES ('24', '系统登陆', null, 'ip:127.0.0.1', '1', '爱吃猫的鱼', '2016-08-17 17:22:22', '2016-08-17 17:22:22');
INSERT INTO `t_log` VALUES ('25', '系统登陆', null, 'ip:127.0.0.1', '1', '爱吃猫的鱼', '2016-08-18 12:45:17', '2016-08-18 12:45:17');
INSERT INTO `t_log` VALUES ('26', '系统登陆', null, 'ip:127.0.0.1', '1', '爱吃猫的鱼', '2016-08-18 13:06:39', '2016-08-18 13:06:39');
INSERT INTO `t_log` VALUES ('27', '系统登陆', null, 'ip:127.0.0.1', '1', '爱吃猫的鱼', '2016-08-18 15:14:16', '2016-08-18 15:14:16');
INSERT INTO `t_log` VALUES ('28', '系统登陆', null, 'ip:127.0.0.1', '1', '爱吃猫的鱼', '2016-08-18 15:25:36', '2016-08-18 15:25:36');
INSERT INTO `t_log` VALUES ('29', '系统登陆', null, 'ip:127.0.0.1', '1', '爱吃猫的鱼', '2016-08-18 16:12:07', '2016-08-18 16:12:07');
INSERT INTO `t_log` VALUES ('30', '系统登陆', null, 'ip:127.0.0.1', '1', '爱吃猫的鱼', '2016-08-18 16:36:36', '2016-08-18 16:36:36');
INSERT INTO `t_log` VALUES ('31', '系统登陆', null, 'ip:127.0.0.1', '1', '爱吃猫的鱼', '2016-08-28 14:47:13', '2016-08-28 14:47:13');
INSERT INTO `t_log` VALUES ('32', '系统登陆', null, 'ip:127.0.0.1', '1', '爱吃猫的鱼1', '2016-08-29 09:13:35', '2016-08-29 09:13:35');
INSERT INTO `t_log` VALUES ('33', '系统登陆', null, 'ip:127.0.0.1', '1', '爱吃猫的鱼1', '2016-08-29 10:11:48', '2016-08-29 10:11:48');
INSERT INTO `t_log` VALUES ('34', '系统登陆', null, 'ip:127.0.0.1', '1', '爱吃猫的鱼1', '2016-08-29 12:57:02', '2016-08-29 12:57:02');
INSERT INTO `t_log` VALUES ('35', '系统登陆', null, 'ip:127.0.0.1', '1', '爱吃猫的鱼1', '2016-08-29 17:27:14', '2016-08-29 17:27:14');
INSERT INTO `t_log` VALUES ('36', '系统登陆', null, 'ip:127.0.0.1', '1', '爱吃猫的鱼1', '2016-08-29 18:35:20', '2016-08-29 18:35:20');

-- ----------------------------
-- Table structure for t_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_menu`;
CREATE TABLE `t_menu` (
  `record_id` int(32) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `url` varchar(50) NOT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `modify_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_menu
-- ----------------------------
INSERT INTO `t_menu` VALUES ('1', '首页', '/article/list.do', 'HOME', '2016-08-29 18:58:59', '2016-08-29 18:58:59');

-- ----------------------------
-- Table structure for t_privilege
-- ----------------------------
DROP TABLE IF EXISTS `t_privilege`;
CREATE TABLE `t_privilege` (
  `record_id` int(10) NOT NULL AUTO_INCREMENT,
  `pid` int(10) DEFAULT NULL,
  `privilege_code` varchar(20) NOT NULL,
  `privilege_name` varchar(200) NOT NULL,
  `url` varchar(200) DEFAULT NULL,
  `remark` varchar(200) DEFAULT NULL,
  `icon_cls` varchar(20) DEFAULT NULL,
  `resourcetype` int(1) DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `modify_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_privilege
-- ----------------------------
INSERT INTO `t_privilege` VALUES ('1', null, 'ITFARM001', '管理员页面', '/asd', null, 'icon-company', '0', '2016-08-16 15:13:49', '2016-07-06 19:19:02');
INSERT INTO `t_privilege` VALUES ('2', null, 'ITFARM005', '系统配置', '/asd', null, 'icon-company', '0', '2016-08-16 15:13:50', '2016-07-06 19:19:14');
INSERT INTO `t_privilege` VALUES ('11', '1', 'ITFARM001001', '药品目录', '/asd', null, 'icon-folder', '1', '2016-08-16 15:13:51', '2016-07-06 19:19:05');
INSERT INTO `t_privilege` VALUES ('12', '1', 'ITFARM001002', '材料目录', '/asd', null, 'icon-folder', '1', '2016-08-16 15:13:52', '2016-07-06 19:19:07');
INSERT INTO `t_privilege` VALUES ('13', '1', 'ITFARM001003', '疾病目录', '/asd', null, 'icon-folder', '1', '2016-08-16 15:13:53', '2016-07-06 19:19:10');

-- ----------------------------
-- Table structure for t_role
-- ----------------------------
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role` (
  `record_id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `remark` varchar(200) DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modify_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_role
-- ----------------------------
INSERT INTO `t_role` VALUES ('1', '用户', '普通用户', '2016-08-15 15:24:33', '2016-08-15 15:24:32');
INSERT INTO `t_role` VALUES ('2', '管理员', '管理员', '2016-08-15 15:24:36', '2016-08-15 15:24:34');
INSERT INTO `t_role` VALUES ('3', '超级管理员', '拥有一切权限', '2016-08-16 10:31:39', '2016-08-16 10:31:39');
INSERT INTO `t_role` VALUES ('6', '管理', '123', '2016-08-29 09:14:24', '2016-08-29 09:14:26');
INSERT INTO `t_role` VALUES ('7', '212', '超级管理员', '2016-08-29 09:14:30', '2016-08-29 09:14:28');
INSERT INTO `t_role` VALUES ('8', '213', '管理', '2016-08-29 09:14:19', '2016-08-29 09:14:22');
INSERT INTO `t_role` VALUES ('12', '135', '管理员', '2016-08-24 11:12:46', '2016-08-17 16:58:12');
INSERT INTO `t_role` VALUES ('13', 'wetew', '23', '2016-08-17 17:02:00', '2016-08-17 17:02:00');

-- ----------------------------
-- Table structure for t_role_privilege
-- ----------------------------
DROP TABLE IF EXISTS `t_role_privilege`;
CREATE TABLE `t_role_privilege` (
  `record_id` int(10) NOT NULL AUTO_INCREMENT,
  `role_id` int(10) NOT NULL,
  `privilege_id` int(10) NOT NULL,
  `privilege_code` varchar(20) DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_role_privilege
-- ----------------------------
INSERT INTO `t_role_privilege` VALUES ('1', '1', '2', 'ITFARM002', '2016-07-06 18:54:15');
INSERT INTO `t_role_privilege` VALUES ('2', '1', '3', 'ITFARM003', '2016-07-06 18:54:15');
INSERT INTO `t_role_privilege` VALUES ('3', '1', '4', 'ITFARM004', '2016-07-06 18:54:15');
INSERT INTO `t_role_privilege` VALUES ('4', '2', '1', 'ITFARM001', '2016-07-06 18:54:15');
INSERT INTO `t_role_privilege` VALUES ('5', '2', '2', 'ITFARM002', '2016-07-06 18:54:15');
INSERT INTO `t_role_privilege` VALUES ('6', '2', '3', 'ITFARM003', '2016-07-06 18:54:15');
INSERT INTO `t_role_privilege` VALUES ('7', '2', '4', 'ITFARM004', '2016-07-06 18:54:15');
INSERT INTO `t_role_privilege` VALUES ('8', '2', '5', 'ITFARM005', '2016-07-06 18:54:15');

-- ----------------------------
-- Table structure for t_system_config
-- ----------------------------
DROP TABLE IF EXISTS `t_system_config`;
CREATE TABLE `t_system_config` (
  `record_id` int(32) NOT NULL AUTO_INCREMENT,
  `web_name` varchar(20) NOT NULL COMMENT 'web项目名',
  `admin_name` varchar(20) NOT NULL COMMENT 'admin项目名',
  `web_title` varchar(100) NOT NULL COMMENT 'web标题',
  `admin_title` varchar(100) NOT NULL COMMENT '后台标题',
  `web_menu_count` int(32) NOT NULL COMMENT 'web菜单数',
  `admin_menu_count` int(32) NOT NULL COMMENT '后台菜单数',
  `article_count` int(32) NOT NULL COMMENT '文章数量',
  `language` varchar(100) NOT NULL COMMENT '系统语言',
  `encoding` varchar(20) NOT NULL COMMENT '编码',
  `modify_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '安装时间',
  `last_modify_user` varchar(100) DEFAULT NULL COMMENT '最后修改人',
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_system_config
-- ----------------------------
INSERT INTO `t_system_config` VALUES ('1', 'itfarm-web', 'itfarm-admin', '程序猿的世界，你不懂', 'ITFARM后台管理中心', '0', '0', '0', 'zh_cn', 'UTF-8', '2016-08-29 10:37:40', '2016-08-29 10:37:43', '爱吃猫的鱼');

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `record_id` int(32) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) NOT NULL COMMENT '用户名',
  `telephone` varchar(11) DEFAULT NULL COMMENT '手机号码',
  `sex` int(1) NOT NULL COMMENT '性别',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `age` int(3) DEFAULT NULL COMMENT '年龄',
  `status` int(1) NOT NULL COMMENT '状态（1、正常 2、停用）',
  `address` varchar(200) DEFAULT NULL COMMENT '地址',
  `photo` varchar(200) DEFAULT NULL COMMENT '头像',
  `email` varchar(200) DEFAULT NULL COMMENT '邮箱',
  `qq` varchar(200) DEFAULT NULL COMMENT 'qq',
  `nickname` varchar(200) DEFAULT NULL COMMENT '昵称',
  `register_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `modify_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('1', 'wdd', '18888888888', '2', '202CB962AC59075B964B07152D234B70', '22', '1', '安徽灵璧1', null, 'asdds', '812908087', '爱吃猫的鱼1', '2016-08-28 14:48:02', '2016-08-28 14:48:01');
INSERT INTO `t_user` VALUES ('6', 'ddw', '13475859654', '1', 'CAF1A3DFB505FFED0D024130F58C5CFA', '21', '1', '浙江杭州', null, 'asdds', '', '爱吃猫的老鼠', '2016-08-16 08:57:59', '2016-08-16 08:57:59');
INSERT INTO `t_user` VALUES ('7', 'wangdongdong', '18395585309', '1', '81DC9BDB52D04DC20036DBD8313ED055', '22', '1', '安徽宿州', null, 'wdd1029@126.com', '812908087', 'wdd', '2016-08-16 08:58:09', '2016-08-16 08:58:09');
INSERT INTO `t_user` VALUES ('8', 'wangdongdong', '18395585309', '1', '81DC9BDB52D04DC20036DBD8313ED055', '22', '1', '安徽宿州', null, 'wdd1029@126.com', '812908087', 'wdd', '2016-08-16 08:58:14', '2016-08-16 08:58:14');
INSERT INTO `t_user` VALUES ('11', 'wdd222', '18888888888', '1', 'DBC4D84BFCFE2284BA11BEFFB853A8C4', '22', '2', '安徽灵璧', null, 'asdds', '812908087', '爱吃猫的鱼', '2016-08-16 08:58:23', '2016-08-16 08:58:23');

-- ----------------------------
-- Table structure for t_user_role
-- ----------------------------
DROP TABLE IF EXISTS `t_user_role`;
CREATE TABLE `t_user_role` (
  `record_id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `role_id` int(10) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_user_role
-- ----------------------------
INSERT INTO `t_user_role` VALUES ('1', '1', '1', '2016-07-06 19:18:40');
INSERT INTO `t_user_role` VALUES ('2', '6', '2', '2016-07-06 19:18:43');
