<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/layouts/basic.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title> ${config.webTitle} </title>
<link href="${ctx }/css/base.css" rel="stylesheet">
<link href="${ctx }/css/index.css" rel="stylesheet">
</head>
<body>
<jsp:include page="../index/header.jsp"/>
<header>
  <div id="logo"><a href="${ctx }/"></a></div>
</header>
<jsp:include page="../index/slider.jsp"></jsp:include>
<article>
  <h2 class="title_tj">
    <p>文章<span>推荐</span></p>
  </h2>
  <div class="bloglist left">
  	<c:forEach items="${articles }" var="item">
    <h3><a href="${ctx }/article/detail.do?id=${item.article.recordId}">${item.article.title }</a></h3>
    <c:if test="${item.article.titleImg != '001.png' && item.titleImgIsExist}">
    <figure><img src="${ctx }/titleImg/${item.article.titleImg}"></figure>
    </c:if>
    <c:if test="${item.article.titleImg == '001.png' || !item.titleImgIsExist}">
    <figure><img src="${ctx }/images/001.png"></figure>
    </c:if>
    <ul>
      <p><itfarm:StringCut length="250" strValue="${item.article.content }"></itfarm:StringCut></p>
      <a title="/" href="${ctx }/article/detail.do?id=${item.article.recordId}" class="readmore">阅读全文>></a>
    </ul>
    <p class="dateview"><span>${item.lastDate }</span><span>作者：${item.authorName }</span><span>分类：[<a href="/news/life/">${item.categoryName }</a>]</span><span>浏览：( <a>${item.article.pageView }</a> )</span></p>
  	</c:forEach>
  	<itfarm:PageBar pageUrl="/article/list.do" pageAttrKey="page"></itfarm:PageBar>
  </div>
  <jsp:include page="../index/right.jsp"></jsp:include>
</article>
  
<footer>
  <p>Design by WindyDriven <a href="http://user.qzone.qq.com/812908087/" title="WindyDriven" target="_blank">WindyDriven</a> <a href="/">网站统计</a></p>
</footer>
<script src="${ctx }/js/silder.js"></script>
</body>
</html>