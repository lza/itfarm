<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/layouts/basic.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
</head>
<body>
<div id="dcWrap">
 <jsp:include page="../header.jsp"></jsp:include>
 ${formToken}
 <div id="dcMain">
   <!-- 当前位置 -->
<div id="urHere">DouPHP 管理中心<b>></b><strong>添加文章</strong> </div>   <div class="mainBox" style="height:auto!important;height:550px;min-height:550px;">
            <h3><a href="${ctx }/article/articleList.do" class="actionBtn">文章列表</a>添加文章</h3>
    <form name="article_form" action="${ctx }/article/addArticle.do" method="post" enctype="multipart/form-data">
     <table width="100%" border="0" cellpadding="8" cellspacing="0" class="tableBasic">
      <tr>
       <td width="90" align="right">文章名称</td>
       <td>
        <input type="text" name="title" value="${article.title }" size="80" class="inpMain" />
       </td>
      </tr>
      <tr>
       <td align="right">文章分类</td>
       <td>
        <select name="typeId">
         <option value="0">未分类</option>
         <c:forEach items="${categorys }" var="item">
         	 <option value="${item.recordId }" <c:if test="${item.recordId==article.typeId }">selected='selected'</c:if>> ${item.name }</option>
         </c:forEach>
          </select>
       </td>
      </tr>
            <tr>
       <td align="right" valign="top">文章描述</td>
       <td>
        <!-- KindEditor -->
        
			<script charset="utf-8" src="${ctx }/js/kindeditor/kindeditor.js"></script>
			<script charset="utf-8" src="${ctx }/js/kindeditor/lang/zh_CN.js"></script>
			<script charset="utf-8" src="${ctx }/js/kindeditor/plugins/code/prettify.js"></script>
        <script>
					KindEditor.ready(function(K) {
						var editor1 = K.create('textarea[name="content"]', {
							cssPath : '${ctx }/js/kindeditor/plugins/code/prettify.css',
							uploadJson : '${ctx }/photo/upload.do',
							fileManagerJson : '${ctx }/photo/manager.do',
							allowFileManager : true,
							afterCreate : function() {
								var self = this;
								K.ctrl(document, 13, function() {
									self.sync();
									K('form[name=article_form]')[0].submit();
								});
								K.ctrl(self.edit.doc, 13, function() {
									self.sync();
									K('form[name=article_form]')[0].submit();
								});
							}
						});
						prettyPrint();
					});
			</script>
        <!-- /KindEditor -->
        <textarea id="content" name="content" style="width:780px;height:400px;" class="textArea">${article.content }</textarea>
       </td>
      </tr>
      <tr>
       <td align="right">缩略图</td>
       <td>
        <input type="file" name="titleImg" size="38" class="inpFlie" />
        <img src="${ctx }/images/icon_no.png"></td>
      </tr>
      <tr>
       <td align="right">关键字</td>
       <td>
        <input type="text" name="keyword" value="${article.keyword }" size="50" class="inpMain" />
       </td>
      </tr>
      <tr>
       <td></td>
       <td>
        <input type="hidden" name="token" value="7e4a88fb" />
        <input type="hidden" name="image" value="${article.titleImg }">
        <input type="hidden" name="recordId" value="${article.recordId }">
        <input name="submit" class="btn" type="submit" value="提交" />
       </td>
      </tr>
     </table>
    </form>
       </div>
 </div>
 <div class="clear"></div>
<jsp:include page="../footer.jsp"></jsp:include>
<div class="clear"></div> </div>
 <!-- <script type="text/javascript">
 
 onload = function()
 {
   document.forms['action'].reset();
 }

 function douAction()
 {
     var frm = document.forms['action'];

     frm.elements['new_cat_id'].style.display = frm.elements['action'].value == 'category_move' ? '' : 'none';
 }
 
 </script> -->
</body>
</html>