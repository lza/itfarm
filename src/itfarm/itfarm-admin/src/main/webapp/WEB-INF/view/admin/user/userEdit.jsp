<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/layouts/basic.jsp" %>
<script type="text/javascript">
    $(function() {
        <%--var roleIds = ${roleIds };
        $('#organizationId').combotree({
            url : '${path }/organization/tree',
            parentField : 'pid',
            lines : true,
            panelHeight : 'auto',
            value : '${user.organizationId}'
        });

        $('#roleIds').combotree({
            url : '${path }/role/tree',
            parentField : 'pid',
            lines : true,
            panelHeight : 'auto',
            multiple : true,
            required : true,
            cascadeCheck : false,
            value : roleIds
        });--%>

        $('#userEditForm').form({
            url : '${path }/user/save.do',
            onSubmit : function() {
                progressLoad();
                var isValid = $(this).form('validate');
                if (!isValid) {
                    progressClose();
                }
                return isValid;
            },
            success : function(result) {
                progressClose();
                result = $.parseJSON(result);
                if (result.success) {
                    parent.$.modalDialog.openner_dataGrid.datagrid('reload');//之所以能在这里调用到parent.$.modalDialog.openner_dataGrid这个对象，是因为user.jsp页面预定义好了
                    parent.$.modalDialog.handler.dialog('close');
                } else {
                    parent.$.messager.alert('错误', result.msg, 'error');
                }
            }
        });
        <%--$("#sex").val('${user.sex}');--%>
        <%--$("#usertype").val('${user.usertype}');--%>
        <%--$("#status").val('${user.status}');--%>
    });
</script>
<div class="easyui-layout" data-options="fit:true,border:false">
    <div data-options="region:'center',border:false" title="" style="overflow: hidden;padding: 3px;">
        <form id="userEditForm" method="post">
            <div class="light-info" style="overflow: hidden;padding: 3px;">
                <div>密码不修改请留空。</div>
            </div>
            <input type="hidden" name="recordId" value="${user.recordId}">
            <table class="grid">
                <tr>
                    <td>登录名</td>
                    <td><input name="username" type="text" placeholder="请输入登录名称" class="easyui-validatebox" data-options="required:true" value="${user.username}"></td>
                    <td>昵称</td>
                    <td><input name="nickname" type="text" placeholder="请输入昵称" class="easyui-validatebox" data-options="required:true" value="${user.nickname}"></td>
                </tr>
                <tr>
                    <td>密码</td>
                    <td><input name="password" type="password" placeholder="不修改则不输入" data-options="required:true"></td>
                    <td>性别</td>
                    <td>
                        <select name="sex" class="easyui-combobox" data-options="width:140,height:29,editable:false,panelHeight:'auto'">
                            <option value="1" <c:if test="${user.sex == 1}">selected="selected"</c:if>>男</option>
                            <option value="2" <c:if test="${user.sex == 2}">selected="selected"</c:if>>女</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>年龄</td>
                    <td><input type="text" name="age" value="${user.age}" class="easyui-numberbox"/></td>
                    <td>手机</td>
                    <td>
                        <input type="text" name="telephone" value="${user.telephone}" class="easyui-numberbox"/>
                    </td>
                </tr>
                <tr>
                    <td>住址</td>
                    <td>
                        <input name="address" type="text" placeholder="请输入住址" data-options="required:true" value="${user.address}">
                    </td>

                    <td>邮箱</td>
                    <td><input name="email" type="text" placeholder="请输入邮箱" data-options="required:true" value="${user.email}"></td>
                </tr>
                <tr>
                    <td>QQ</td>
                    <td>
                        <input type="text" name="qq" value="${user.qq}" class="easyui-numberbox"/>
                    </td>
                    <td>用户状态</td>
                    <td>
                        <select id="status" name="status" class="easyui-combobox" data-options="width:140,height:29,editable:false,panelHeight:'auto'">
                            <option value="1" <c:if test="${user.status == 1}">selected="selected"</c:if> >正常</option>
                            <option value="2" <c:if test="${user.status == 2}">selected="selected"</c:if>>停用</option>
                        </select>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>