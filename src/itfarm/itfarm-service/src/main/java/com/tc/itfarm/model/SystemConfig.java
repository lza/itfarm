package com.tc.itfarm.model;

import java.io.Serializable;
import java.util.Date;

public class SystemConfig implements Serializable {
    private Integer recordId;

    private String webName;

    private String adminName;

    private String webTitle;

    private String adminTitle;

    private Integer webMenuCount;

    private Integer adminMenuCount;

    private Integer articleCount;

    private String language;

    private String encoding;

    private Date modifyTime;

    private Date createTime;

    private String lastModifyUser;

    private static final long serialVersionUID = 1L;

    public SystemConfig(Integer recordId, String webName, String adminName, String webTitle, String adminTitle, Integer webMenuCount, Integer adminMenuCount, Integer articleCount, String language, String encoding, Date modifyTime, Date createTime, String lastModifyUser) {
        this.recordId = recordId;
        this.webName = webName;
        this.adminName = adminName;
        this.webTitle = webTitle;
        this.adminTitle = adminTitle;
        this.webMenuCount = webMenuCount;
        this.adminMenuCount = adminMenuCount;
        this.articleCount = articleCount;
        this.language = language;
        this.encoding = encoding;
        this.modifyTime = modifyTime;
        this.createTime = createTime;
        this.lastModifyUser = lastModifyUser;
    }

    public SystemConfig() {
        super();
    }

    public Integer getRecordId() {
        return recordId;
    }

    public void setRecordId(Integer recordId) {
        this.recordId = recordId;
    }

    public String getWebName() {
        return webName;
    }

    public void setWebName(String webName) {
        this.webName = webName == null ? null : webName.trim();
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName == null ? null : adminName.trim();
    }

    public String getWebTitle() {
        return webTitle;
    }

    public void setWebTitle(String webTitle) {
        this.webTitle = webTitle == null ? null : webTitle.trim();
    }

    public String getAdminTitle() {
        return adminTitle;
    }

    public void setAdminTitle(String adminTitle) {
        this.adminTitle = adminTitle == null ? null : adminTitle.trim();
    }

    public Integer getWebMenuCount() {
        return webMenuCount;
    }

    public void setWebMenuCount(Integer webMenuCount) {
        this.webMenuCount = webMenuCount;
    }

    public Integer getAdminMenuCount() {
        return adminMenuCount;
    }

    public void setAdminMenuCount(Integer adminMenuCount) {
        this.adminMenuCount = adminMenuCount;
    }

    public Integer getArticleCount() {
        return articleCount;
    }

    public void setArticleCount(Integer articleCount) {
        this.articleCount = articleCount;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language == null ? null : language.trim();
    }

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding == null ? null : encoding.trim();
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getLastModifyUser() {
        return lastModifyUser;
    }

    public void setLastModifyUser(String lastModifyUser) {
        this.lastModifyUser = lastModifyUser == null ? null : lastModifyUser.trim();
    }
}