package com.tc.itfarm.dao;

import com.tc.itfarm.api.model.SingleTableDao;
import com.tc.itfarm.model.Role;
import com.tc.itfarm.model.RoleCriteria;

public interface RoleDao extends SingleTableDao<Role, RoleCriteria> {
}