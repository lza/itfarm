package com.tc.itfarm.service.impl;

import com.tc.itfarm.api.model.SingleTableDao;
import com.tc.itfarm.dao.UserRoleDao;
import com.tc.itfarm.model.UserRole;
import com.tc.itfarm.model.UserRoleCriteria;
import com.tc.itfarm.service.UserRoleService;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UserRoleServiceImpl extends BaseServiceImpl<UserRole> implements UserRoleService {

	@Resource
	private UserRoleDao userRoleDao;

	@Override
	public List<UserRole> selectByUserId(Integer userId) {
		Assert.isTrue(userId != null, "userId不能为空");
		UserRoleCriteria criteria = new UserRoleCriteria();
		criteria.or().andUserIdEqualTo(userId);
		return userRoleDao.selectByCriteria(criteria);
	}

	@Override
	protected SingleTableDao getSingleDao() {
		return userRoleDao;
	}
}
