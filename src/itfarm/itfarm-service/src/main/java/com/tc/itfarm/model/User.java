package com.tc.itfarm.model;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {
    private Integer recordId;

    private String username;

    private String telephone;

    private Integer sex;

    private String password;

    private Integer age;

    private Integer status;

    private String address;

    private String photo;

    private String email;

    private String qq;

    private String nickname;

    private Date registerTime;

    private Date modifyTime;

    private static final long serialVersionUID = 1L;

    public User(Integer recordId, String username, String telephone, Integer sex, String password, Integer age, Integer status, String address, String photo, String email, String qq, String nickname, Date registerTime, Date modifyTime) {
        this.recordId = recordId;
        this.username = username;
        this.telephone = telephone;
        this.sex = sex;
        this.password = password;
        this.age = age;
        this.status = status;
        this.address = address;
        this.photo = photo;
        this.email = email;
        this.qq = qq;
        this.nickname = nickname;
        this.registerTime = registerTime;
        this.modifyTime = modifyTime;
    }

    public User() {
        super();
    }

    public Integer getRecordId() {
        return recordId;
    }

    public void setRecordId(Integer recordId) {
        this.recordId = recordId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone == null ? null : telephone.trim();
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo == null ? null : photo.trim();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq == null ? null : qq.trim();
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname == null ? null : nickname.trim();
    }

    public Date getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(Date registerTime) {
        this.registerTime = registerTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }
}