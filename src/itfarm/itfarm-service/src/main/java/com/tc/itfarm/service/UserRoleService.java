package com.tc.itfarm.service;

import java.util.List;

import com.tc.itfarm.model.UserRole;

public interface UserRoleService extends BaseService<UserRole> {
	List<UserRole> selectByUserId(Integer userId);
}
