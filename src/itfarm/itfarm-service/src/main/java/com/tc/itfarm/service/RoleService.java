package com.tc.itfarm.service;


import java.util.Date;
import java.util.List;

import com.tc.itfarm.api.model.Page;
import com.tc.itfarm.api.model.PageList;
import com.tc.itfarm.model.Role;

public interface RoleService extends BaseService<Role> {
	Integer save(Role role);

	List<Role> selectByUserId(Integer userId);

	PageList<Role> selectByPageList(String name, Date startDate, Date endDate, Page page);

}
