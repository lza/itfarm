package com.tc.itfarm.commons.web.security;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.SecurityMetadataSource;
import org.springframework.security.access.intercept.AbstractSecurityInterceptor;
import org.springframework.security.access.intercept.InterceptorStatusToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.stereotype.Service;
/**
 * 
 * @Description: 登陆验证拦截器
 * @author: wangdongdong  
 * @date:   2016年7月1日 上午9:04:06   
 *
 */
//@Service("commonFilterSecurityInterceptor")
public class CommonFilterSecurityInterceptor extends AbstractSecurityInterceptor implements Filter{

	@Resource
	private FilterInvocationSecurityMetadataSource commonInvocationSecurityMetadataSource;
	
	
	@Override
	@Resource(name = "commonAccessDecisionManager")
	public void setAccessDecisionManager(AccessDecisionManager accessDecisionManager) {
		super.setAccessDecisionManager(accessDecisionManager);
	}

	@Override
	@Resource
	public void setAuthenticationManager(AuthenticationManager authenticationManager) {
		super.setAuthenticationManager(authenticationManager);
	}

	@Override
	public Class<? extends Object> getSecureObjectClass() {
		return FilterInvocation.class;
	}

	@Override
	public SecurityMetadataSource obtainSecurityMetadataSource() {
		return this.commonInvocationSecurityMetadataSource;
	}

	public void destroy() {
		
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		FilterInvocation filterInvocation = new FilterInvocation(request, response, chain);
		
		InterceptorStatusToken token = super.beforeInvocation(filterInvocation);
		filterInvocation.getChain().doFilter(request, response);
		super.afterInvocation(token, null);
	}

	public void init(FilterConfig arg0) throws ServletException {
		
	}

	public FilterInvocationSecurityMetadataSource getSecurityMetadataSource() {
		return commonInvocationSecurityMetadataSource;
	}

	public void setSecurityMetadataSource(FilterInvocationSecurityMetadataSource securityMetadataSource) {
		this.commonInvocationSecurityMetadataSource = securityMetadataSource;
	}
	
}
